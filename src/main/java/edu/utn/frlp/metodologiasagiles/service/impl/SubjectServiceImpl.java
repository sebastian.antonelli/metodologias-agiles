package edu.utn.frlp.metodologiasagiles.service.impl;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import edu.utn.frlp.metodologiasagiles.entity.Student;
import edu.utn.frlp.metodologiasagiles.entity.Subject;
import edu.utn.frlp.metodologiasagiles.repository.SubjectRepository;
import edu.utn.frlp.metodologiasagiles.service.StudentService;
import edu.utn.frlp.metodologiasagiles.service.SubjectService;

@Service("subjectService")
public class SubjectServiceImpl implements SubjectService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SubjectServiceImpl.class);
    
    
    @Autowired
    SubjectRepository subjectRepository;
    
    
    @Autowired
   	StudentService studentService;
    
    
	@Override
	public Iterable<Subject> findAll() {
		return subjectRepository.findAll();
	}

	
	@Override
	public Subject save(Subject subject) {
		return subjectRepository.save(subject);
	}

	
	@Override
	public Optional<Subject> findById(long id) {
		return subjectRepository.findById(id);
	}

	
	@Override
	public void delete(Subject subject) {
		subjectRepository.delete(subject);
	}
	
	
	@Override
	public boolean isAlreadyExistingSubject(String name) {
		LOGGER.info("Verifiying existing subject by name {}", name);
        Optional<List<Subject>> subjectOpt = subjectRepository.findByName(name);
        return subjectOpt.isPresent();
	} 
	
	
	@Override
	public boolean isAlreadyExistingSubject_Id(long id) {
		LOGGER.info("Verifiying existing Subject by id {}", id);
        Optional<Subject> SubjectOpt = subjectRepository.findById(id);
        return SubjectOpt.isPresent();
	}
	
	
	@Override
	public void subjectInscription(long studentId, long subjectId) {
		try {
			Subject subject = this.findById(subjectId).get();
	    	Student student = studentService.findById(studentId).get();
	    	if ((student.getSubjects() != null) && (student.getSubjects().size()>0)) {
	    		boolean found = false;

        		Iterator<Subject> it=student.getSubjects().iterator();

	    		while (!found && it.hasNext()) {
	    			Subject subjectForStudent=(Subject) it.next();
	    			if (subjectForStudent.getId() == subjectId) {
	    				found=true;
	    				throw new EntityNotFoundException("You are already enrolled in the subject with id " + subjectId + " and name "+subject.getName());
	    			}	
	    		}
	    	}
	    	
	    	if(student.getSubjects() == null) {
	    		student.setSubjects(new HashSet<Subject>());
	    	}
	    	
	    	student.getSubjects().add(subject);   
	    	if(subject.getStudents() == null) {
		    	subject.setStudents(new HashSet<Student>());
		    }
	    	
    		subject.getStudents().add(student);
    		studentService.save(student);
    		
		} catch (Exception e) {
			LOGGER.error("Error Message: " + e.getMessage());
			throw e;
		}
	}
	
	
	@Override
	public void unsubscribeSubject(long studentId, long subjectId) {
		try {
			Subject subject = this.findById(subjectId).get();
	    	Student student = studentService.findById(studentId).get();
	  
	    	if ((student.getSubjects() != null) && (student.getSubjects().size()>0) && (student.getSubjects().contains(subject))) {
	    		student.getSubjects().remove(subject); 
    	    	subject.getStudents().remove(student);
    	    	studentService.save(student);
    	    	this.save(subject);
	    	} else {
	    	    throw new EntityNotFoundException("You are already unsubscribed of the subject with id " + subjectId + " and name "+subject.getName());
	       	}
	    	
		} catch (Exception e) {
			LOGGER.error("Error Message: " + e.getMessage());
			throw e;
		}
	}
	
}