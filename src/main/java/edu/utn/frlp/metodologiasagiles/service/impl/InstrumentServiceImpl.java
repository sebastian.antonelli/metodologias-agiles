package edu.utn.frlp.metodologiasagiles.service.impl;

import java.util.ArrayList;
import java.util.List;

import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.utn.frlp.metodologiasagiles.entity.Institute;
import edu.utn.frlp.metodologiasagiles.entity.Instrument;
import edu.utn.frlp.metodologiasagiles.entity.Student;

import edu.utn.frlp.metodologiasagiles.repository.InstrumentRepository;
import edu.utn.frlp.metodologiasagiles.service.InstituteService;
import edu.utn.frlp.metodologiasagiles.service.InstrumentService;
import edu.utn.frlp.metodologiasagiles.service.StudentService;

@Service("instrumentService")
public class InstrumentServiceImpl implements InstrumentService {

   private static final Logger LOGGER = LoggerFactory.getLogger(InstrumentServiceImpl.class);
    
    @Autowired
    InstrumentRepository instrumentRepository;
    
    @Autowired
    InstituteService instituteService;
    
    @Autowired
   	StudentService studentService;

	@Override
	public Iterable<Instrument> findAll() {
		return instrumentRepository.findAll();
	}

	@Override
	public boolean isAlreadyExistingInstrument_id(long id) {
		LOGGER.info("Verifiying existing instrument by id {}", id);
        Optional<Instrument> instrumentOpt = instrumentRepository.findById(id);
        return instrumentOpt.isPresent();
	}

	@Override
	public void rentInstrument(long studentId, long instrumentId) {
		try {
			Instrument instrument = this.findById(instrumentId).get();
	    	Student student = studentService.findById(studentId).get();
	    	if(instrument.isState()) {
	    		if(student.getInstrumets() == null) {
	    			student.setInstrumets(new ArrayList<Instrument>());
	    		}
	    		instrument.setState(false);
	    		instrument.setStudent(student);
	    		instrument.setRentalCount(instrument.getRentalCount()+1);
	    		student.setSpentMoney(student.getSpentMoney()+instrument.getRentalRate());
	    		student.getInstrumets().add(instrument);
	    		studentService.save(student);
	    		Institute institute = instituteService.findById(1L).get();
	    		institute.setTotalRentals(institute.getTotalRentals()+instrument.getRentalRate());
	    		instituteService.save(institute);
	    	} else {
	    		throw new EntityNotFoundException("The instrument with Id " + instrumentId + " and name "+instrument.getName()+" is already rented");
	    	}
		} catch (Exception e) {
			LOGGER.error("Error Message: " + e.getMessage());
			throw e;
		}
	}
	
	@Override
	public void returnInstrument(long studentId, long instrumentId) {
		try {
			Instrument instrument = this.findById(instrumentId).get();
			Student student = studentService.findById(studentId).get();
			if(!instrument.isState()) {
				if(!student.getInstrumets().isEmpty()) {
					if (student.getInstrumets().contains(instrument)) {
						instrument.setState(true);
						instrument.setStudent(null);
						student.getInstrumets().remove(instrument);
						studentService.save(student);
					} else {
						throw new EntityNotFoundException("The instrument with Id " + instrumentId + " is already rented for another student");
					}
				} else {
					throw new EntityNotFoundException ("The student whit id " + student.getId() + " didn´t rent any instrument");
				}
			} else {
				throw new EntityNotFoundException("The instrument with id " + instrumentId + " and name "+instrument.getName()+" isn´t  already rented");
			}
		} catch (Exception e) {
			LOGGER.error("Error Message: " + e.getMessage());
			throw e;
		}
	}

	public Instrument save(Instrument instrument) {
		return instrumentRepository.save(instrument);
	}

	@Override
	public Optional<Instrument> findById(long id) {
		return instrumentRepository.findById(id);
	}

	@Override
	public void delete(Instrument instrument) {
		instrumentRepository.delete(instrument);
	}

	@Override
	public Iterable<Instrument> findByName(String name) {
		return null;
	}

	@Override
	public boolean isAlreadyExistingInstrument(String name) {
		return false;
	}

	@Override
	public Iterable<Instrument> saveAll(List<Instrument> instrumets) {
		return instrumentRepository.saveAll(instrumets);
	}
}