package edu.utn.frlp.metodologiasagiles.service.impl;

import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.utn.frlp.metodologiasagiles.entity.Student;
import edu.utn.frlp.metodologiasagiles.repository.StudentRepository;
import edu.utn.frlp.metodologiasagiles.service.StudentService;

@Service("studentService")
public class StudentServiceImpl implements StudentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StudentServiceImpl.class);
    
    
    @Autowired
    StudentRepository studentRepository;

	@Override
	public Iterable<Student> findAll() {
		return studentRepository.findAll();
	}

	@Override
	public Student save(Student student) {
		return studentRepository.save(student);
	}

	@Override
	public Iterable<Student> saveAll(Set<Student> students) {
		return studentRepository.saveAll(students);
	}

	@Override
	public Optional<Student> findById(long id) {
		return studentRepository.findById(id);
	}

	@Override
	public void delete(Student student) {
		studentRepository.delete(student);
	}
	
	@Override
	public boolean isAlreadyExistingStudent(long id) {
		LOGGER.info("Verifiying existing Student by id {}", id);
        Optional<Student> StudentOpt = studentRepository.findById(id);
        return StudentOpt.isPresent();
	}
	
	@Override
	public Optional<Student> findByUsername(String username) {
        Optional<Student> StudentOpt = studentRepository.findByUsername(username);
		return StudentOpt;
	}
}