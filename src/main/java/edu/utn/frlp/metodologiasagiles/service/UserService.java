package edu.utn.frlp.metodologiasagiles.service;

import java.util.Optional;

import edu.utn.frlp.metodologiasagiles.entity.User;

public interface UserService {
	
	User findByUsername(String username);
	
	Optional<User> findByEmail(String email);

	boolean isAlreadyExistingUser(String username, String email);
}