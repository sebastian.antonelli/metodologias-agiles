package edu.utn.frlp.metodologiasagiles.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.utn.frlp.metodologiasagiles.entity.Institute;
import edu.utn.frlp.metodologiasagiles.repository.InstituteRepository;
import edu.utn.frlp.metodologiasagiles.service.InstituteService;

@Service("instituteService")
public class InstituteServiceImpl implements InstituteService {

   private static final Logger LOGGER = LoggerFactory.getLogger(InstituteServiceImpl.class);
    
    @Autowired
    InstituteRepository instituteRepository;
    
    @Override
    public Optional<Institute> findById(Long instituteId){
    	LOGGER.info("Retrieving institute by Id: " + instituteId);
    	return instituteRepository.findById(instituteId);
    }

	@Override
	public void save(Institute institute) {
		instituteRepository.save(institute);
	}
}