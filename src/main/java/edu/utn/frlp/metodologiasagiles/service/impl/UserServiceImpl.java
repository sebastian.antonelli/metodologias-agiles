package edu.utn.frlp.metodologiasagiles.service.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.utn.frlp.metodologiasagiles.entity.User;
import edu.utn.frlp.metodologiasagiles.repository.UserRepository;
import edu.utn.frlp.metodologiasagiles.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService {
    
    @Autowired
    UserRepository userRepository;


    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    @Override
    public User findByUsername(String username) throws EntityNotFoundException {
		LOGGER.info("Loading user by username {}", username);
        User user = userRepository.findByUsername(username).orElseThrow(() -> new EntityNotFoundException("The user with username " + username + " doesn't exist in DB."));
        return user;
    }
    
    @Override
    public Optional<User> findByEmail(String email) throws EntityNotFoundException {
		LOGGER.info("Loading user by email {}", email);
        Optional<User> userOpt = userRepository.findByEmail(email);
        return userOpt;
    }

	@Override
	public boolean isAlreadyExistingUser(String username, String email) {
		LOGGER.info("Verifiying existing user by username {} or email {}", username, email);
        Optional<List<User>> userOpt = userRepository.findByUsernameOrEmail(username, email);
        return userOpt.isPresent();
	}
}
