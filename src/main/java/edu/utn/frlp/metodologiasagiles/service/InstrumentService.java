package edu.utn.frlp.metodologiasagiles.service;

import java.util.List;
import java.util.Optional;
import edu.utn.frlp.metodologiasagiles.entity.Instrument;

public interface InstrumentService {

	Iterable<Instrument> findAll();
	
	boolean isAlreadyExistingInstrument_id(long id);
	
	void rentInstrument(long studentId, long instrumentId);

	Instrument save(Instrument instrument);

	Iterable<Instrument> findByName(String name);
	
	Optional<Instrument> findById(long id);

	void delete(Instrument instrument);	
	
	boolean isAlreadyExistingInstrument(String name);
	
	void returnInstrument(long studentId, long instrumentId);

	Iterable<Instrument> saveAll(List<Instrument> instrumets);
}