package edu.utn.frlp.metodologiasagiles.service;

import java.util.Optional;

import edu.utn.frlp.metodologiasagiles.entity.Subject;

public interface SubjectService {

	Iterable<Subject> findAll();

	Subject save(Subject subject);

	Optional<Subject> findById(long id);

	void delete(Subject subject);	
	
	void subjectInscription(long studentId, long subjectId);
	
	void unsubscribeSubject(long studentId, long subjectId);
	
	boolean isAlreadyExistingSubject(String name);
	
	boolean isAlreadyExistingSubject_Id(long id);
	
	
}