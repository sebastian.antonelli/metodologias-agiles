package edu.utn.frlp.metodologiasagiles.service;

import java.util.Optional;

import edu.utn.frlp.metodologiasagiles.entity.Institute;

public interface InstituteService {

	Optional<Institute> findById(Long instituteId);
	
	void save(Institute institute);

}