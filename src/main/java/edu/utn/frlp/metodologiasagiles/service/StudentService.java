package edu.utn.frlp.metodologiasagiles.service;

import java.util.Optional;
import java.util.Set;

import edu.utn.frlp.metodologiasagiles.entity.Student;

public interface StudentService {

	Iterable<Student> findAll();

	Student save(Student student);

	Optional<Student> findById(long id);

	void delete(Student student);	
	
	boolean isAlreadyExistingStudent(long id);
	
	Optional<Student> findByUsername(String username);

	Iterable<Student> saveAll(Set<Student> students);
}