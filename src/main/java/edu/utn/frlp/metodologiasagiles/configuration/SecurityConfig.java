package edu.utn.frlp.metodologiasagiles.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import edu.utn.frlp.metodologiasagiles.security.CustomUserDetailsService;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	CustomUserDetailsService customUserDetailsService;
	
	@Autowired
	DataSource dataSource;

	@Override
	public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
		authenticationManagerBuilder.userDetailsService(customUserDetailsService)
									.passwordEncoder(passwordEncoder());

	}

	@Bean(BeanIds.AUTHENTICATION_MANAGER)
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public AuthenticationTrustResolver getAuthenticationTrustResolver() {
		return new AuthenticationTrustResolverImpl();
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/bootstrap/**",
                                    "/css/**",
                                    "/datatables/**",
                                    "/fonts/**",
                                    "/img/**",
                                    "/js/**");
    }

	@Override
	protected void configure(final HttpSecurity http) throws Exception {
		http.csrf().disable();

		// The pages does not require login
		http.authorizeRequests().antMatchers("/", "/logout", "/access-denied").hasAnyAuthority("STUDENT","TEACHER", "ADMINISTRATOR");
		http.authorizeRequests().antMatchers("/login","/h2-console/*").permitAll();

		// Admin
		http.authorizeRequests().antMatchers("/admin/**").hasAnyAuthority("ADMINISTRATOR");
				
		// Students
		http.authorizeRequests().antMatchers("/students/**").hasAnyAuthority("ADMINISTRATOR");
		
		// Instruments
		http.authorizeRequests().antMatchers("/instruments/**").hasAnyAuthority("STUDENT", "ADMINISTRATOR");
		
		// Subjects
		http.authorizeRequests().antMatchers("/subjects/**").hasAnyAuthority("STUDENT", "ADMINISTRATOR");
		
		http.authorizeRequests().anyRequest().authenticated();
//		http.authorizeRequests().antMatchers("/student").hasAnyAuthority("STUDENT", "ADMINISTRATOR");

		// Config for Login Form
		http.authorizeRequests()
				.and().formLogin()//
				// Submit URL of login page.
				.loginProcessingUrl("/j_spring_security_check") // Submit URL
				.loginPage("/login")//
				.defaultSuccessUrl("/index",true)//
				.failureUrl("/login?error=true")//
				.usernameParameter("username")//
				.passwordParameter("password")
				.and()
				.userDetailsService(customUserDetailsService)
				// Config for Logout Page
				.logout().logoutUrl("/logout").logoutSuccessUrl("/login")
				.and().exceptionHandling().accessDeniedPage("/access-denied");
		
		// add this line to use H2 web console
	    http.headers().frameOptions().disable();
	}

}
