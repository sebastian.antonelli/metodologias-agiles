package edu.utn.frlp.metodologiasagiles.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.utn.frlp.metodologiasagiles.entity.Instrument;

@Repository
public interface InstrumentRepository extends CrudRepository<Instrument, Long> {
    
    Optional<List<Instrument>> findByName(String name);
}    