package edu.utn.frlp.metodologiasagiles.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.utn.frlp.metodologiasagiles.entity.Subject;

@Repository
public interface SubjectRepository extends CrudRepository<Subject, Long> {
    
    Optional<List<Subject>> findByName(String name);
    
}
