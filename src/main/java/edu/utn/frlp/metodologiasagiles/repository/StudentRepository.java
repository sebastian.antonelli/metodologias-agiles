package edu.utn.frlp.metodologiasagiles.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.utn.frlp.metodologiasagiles.entity.Student;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {
    
    List<Student> findByFirstNameAndLastName(String firstName, String lastName);
    
    @Query("select s from Student s JOIN s.user u where lower(u.username) like lower(:username)")
    Optional<Student> findByUsername(String username);
    
}
