package edu.utn.frlp.metodologiasagiles.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.utn.frlp.metodologiasagiles.entity.Institute;

@Repository
public interface InstituteRepository extends CrudRepository<Institute, Long> {
    
}    