package edu.utn.frlp.metodologiasagiles.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.utn.frlp.metodologiasagiles.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    
    Optional<User> findByUsername(String username);
    
    Optional<List<User>> findByUsernameOrEmail(String username, String email);

	Optional<User> findByEmail(String email);
}