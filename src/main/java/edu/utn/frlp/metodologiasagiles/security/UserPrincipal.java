package edu.utn.frlp.metodologiasagiles.security;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import edu.utn.frlp.metodologiasagiles.entity.User;

public class UserPrincipal implements UserDetails {

	private static final long serialVersionUID = 1L;
	private Long id;
	private String username;
	@JsonIgnore
	private String password;

	private static String loggedInUser = "";

	private Collection<? extends GrantedAuthority> authorities;

	private static final Logger LOGGER = LoggerFactory.getLogger(UserPrincipal.class);

	public UserPrincipal(Long id, String username, String password, Collection<? extends GrantedAuthority> authorities) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.authorities = authorities;
	}

	public static UserPrincipal create(User user) {
		LOGGER.info("Creating UserPrincipal for {}", user.getUsername());
		List<GrantedAuthority> authorities = Arrays.asList(new SimpleGrantedAuthority(user.getRole().toString()));
		LOGGER.info("User roles allowed: {}", authorities.toString());
		return new UserPrincipal(
				user.getId(),
				user.getUsername(),
				user.getPassword(),
				authorities
		);
	}

	/**
	 * Returns the username of the logged-in user.
	 * @return username
	 */
	public static String getPrincipal(){
		try {
			String username;
			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if (principal instanceof UserDetails) {
				username = ((UserDetails) principal).getUsername();
			} else {
				username = principal.toString();
			}
			return username;
		} catch (NullPointerException e) {
			LOGGER.warn("There is no authentication!");
			return null;
		}
	}

	public Long getId() { return id; }

	@Override
	public String getUsername() { return username; }

	@Override
	public String getPassword() { return password; }

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() { return authorities; }

	@Override
	public boolean isAccountNonExpired() { return true; }

	@Override
	public boolean isAccountNonLocked() { return true; }

	@Override
	public boolean isCredentialsNonExpired() { return true; }

	@Override
	public boolean isEnabled() { return true; }

	public static String getLoggedInUser() {
		return loggedInUser;
	}

	public static void setLoggedInUser(String loggedInUser) {
		UserPrincipal.loggedInUser = loggedInUser;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		UserPrincipal that = (UserPrincipal) o;
		return Objects.equals(id, that.id);
	}

	@Override
	public int hashCode() {

		return Objects.hash(id);
	}
}