package edu.utn.frlp.metodologiasagiles.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.utn.frlp.metodologiasagiles.entity.User;
import edu.utn.frlp.metodologiasagiles.service.UserService;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	UserService userService;

	private static final Logger LOGGER = LoggerFactory.getLogger(CustomUserDetailsService.class);

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) {
		LOGGER.info("Loading user by username {}", username);
		User user = userService.findByUsername(username);
		return UserPrincipal.create(user);
	}
}