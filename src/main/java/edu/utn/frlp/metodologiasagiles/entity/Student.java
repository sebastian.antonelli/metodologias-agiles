package edu.utn.frlp.metodologiasagiles.entity;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Student {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotBlank(message = "This field is required")
    @Column(name = "first_name")
    private String firstName;
    
    @NotBlank(message = "This field is required")
    @Column(name = "last_name")
    private String lastName;
    
    @NotBlank(message = "This field is required")
    @Column(name = "dni")
    private String dni;
    
    @NotBlank(message = "This field is required")
    @Column(name = "code")
    private String code;
    
    @NotEmpty(message = "This field is required")
    @Column(name = "phoneNo")
    private String phoneNo;
   
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="user_id")
    private User user;
    
    @Column(name = "spent_money")
    private Float spentMoney;
    
    @OneToMany(mappedBy = "student")
    private List<Instrument> instrumets;
    
    @ManyToMany
    @JoinTable(
      name = "students_subjects", 
      joinColumns = @JoinColumn(name = "student_id"), 
      inverseJoinColumns = @JoinColumn(name = "subject_id"))
    Set<Subject> subjects;
}