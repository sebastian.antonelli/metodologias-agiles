package edu.utn.frlp.metodologiasagiles.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class Instrument {

	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private long id;
	    
	    @Column(name = "name")
	    private String name;
	    
	    @Column(name = "rental_rate")
	    private Float rentalRate;

	    @Column(name = "state")
	    private boolean state;
	    
	    @Column(name = "rental_count")
	    private Long rentalCount;
	    
	    @ManyToOne(fetch = FetchType.LAZY)
	    @JoinColumn(name = "student_id")
	    private Student student;	    	    
}

