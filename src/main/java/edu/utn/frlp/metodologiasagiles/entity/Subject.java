package edu.utn.frlp.metodologiasagiles.entity;


import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Subject {

	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;
	    
	    @NotBlank(message = "Name is mandatory")
	    @Column(name = "name")
	    private String name;
	    
	    @Column (name ="days")
	    @ElementCollection(targetClass=String.class)
	    private List<String> days;
	    
	    @NotNull(message = "startTime is mandatory")
	    @Column(name = "start_time")
	    private String startTime;

	    @Column(name = "duration")
	    private String duration;
	    
	    @ManyToMany(mappedBy = "subjects")
	    Set<Student> students;
	    
	    public void addStudent(Student student) {
	        this.students.add(student);
	        student.getSubjects().add(this);
	    }
	  
	    public void removeStudent(Student student) {
	        this.students.remove(student);
	        student.getSubjects().remove(this);
	    }
	    
}
