package edu.utn.frlp.metodologiasagiles.entity;

public enum Role {
	
	STUDENT,
	TEACHER,
	ADMINISTRATOR
}
