package edu.utn.frlp.metodologiasagiles.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RentalDTO {
	
  private String studentName;
  private String username;
  private String email;
  private String instrumentName;
  private Float instrumentCost;

}
