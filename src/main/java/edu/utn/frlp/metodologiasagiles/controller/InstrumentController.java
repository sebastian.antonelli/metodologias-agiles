package edu.utn.frlp.metodologiasagiles.controller;

import static edu.utn.frlp.metodologiasagiles.security.UserPrincipal.getPrincipal;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.utn.frlp.metodologiasagiles.entity.Instrument;
import edu.utn.frlp.metodologiasagiles.entity.Student;
import edu.utn.frlp.metodologiasagiles.service.InstituteService;
import edu.utn.frlp.metodologiasagiles.service.InstrumentService;
import edu.utn.frlp.metodologiasagiles.service.StudentService;

@Controller
@RequestMapping("/instruments/")
public class InstrumentController {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(InstrumentController.class);

    @Autowired
	InstrumentService instrumentService;
    
    @Autowired
	InstituteService instituteService;
    
    @Autowired
 	StudentService studentService;

	@GetMapping("list")
	public String getAllInstruments(Model model) {
		Optional<Student> studentOpt = studentService.findByUsername(getPrincipal());
		List<Long> instrumentsIds = new ArrayList<Long>();
		if (studentOpt.isPresent()) {
			long studentId = studentOpt.get().getId();
			model.addAttribute("studentId", studentId);
			
			for (Instrument instrument : studentOpt.get().getInstrumets()) {
				Long instrumentId= instrument.getId();
				instrumentsIds.add(instrumentId);
			}
		} else {
			model.addAttribute("studentId", 0);
		}
		model.addAttribute("studentInstrumentsIds", instrumentsIds);
		model.addAttribute("instruments", instrumentService.findAll());
		return "instruments/instrument-list";
	}
	
	@GetMapping("rent/{studentId}/{instrumentId}")
	public String rentInstrument(@PathVariable("studentId") long studentId, @PathVariable("instrumentId") long instrumentId, Model model) {
		try {
			
	    	if (!instrumentService.isAlreadyExistingInstrument_id(instrumentId)) {
	        	throw new EntityNotFoundException("The instrument with id " + instrumentId + " doesn´t exist in DB.");
	    	}
	    	
	    	if (!studentService.isAlreadyExistingStudent(studentId)) {
	        	throw new EntityNotFoundException("The student with id " + studentId + " doesn´t exist in DB.");
	    	}
	    	
	    	instrumentService.rentInstrument(studentId, instrumentId);
	    	model.addAttribute("success", "The instrument was rented successfully.");
	        model.addAttribute("url", "instruments/list");
	        return "success";
		} catch (Exception e) {
			LOGGER.error("Error Message: " + e.getMessage());
			throw e;
		}
	}
	
	@GetMapping("return/{studentId}/{instrumentId}")
	public String returnInstrument(@PathVariable("studentId") long studentId, @PathVariable("instrumentId") long instrumentId, Model model) {
		try {
			
			if (!instrumentService.isAlreadyExistingInstrument_id(instrumentId)) {
				throw new EntityNotFoundException("The instrument with id " + instrumentId + " doesn´t exist in DB.");
			}
			
			if (!studentService.isAlreadyExistingStudent(studentId)) {
				throw new EntityNotFoundException("The student whit id " + studentId + " doesn´t exist in DB.");
			}
			
			instrumentService.returnInstrument(studentId, instrumentId);
			model.addAttribute("success", "The instrument was return succesfully");
			model.addAttribute("url", "instruments/list");
			return "success";
		} catch (Exception e) {
			LOGGER.error("Error Message: " + e.getMessage());
			throw e;
		}
	}
	@GetMapping("add")
	public String getAddIntrumentForm(Instrument instrument) {
		return "instruments/add-instrument";
	}
	
	@PostMapping("add")
	public String addInstrument(@Valid Instrument instrument, BindingResult result, Model model) {
		try {
			if (result.hasErrors()) {
				return "instruments/instrument-add";
			}
			instrument.setState(true);
			instrument.setRentalCount(0L);
	    	instrumentService.save(instrument);
			return "redirect:list";
		} catch (Exception e) {
			LOGGER.error("Error Message: " + e.getMessage());
			throw e;
		}
	}

	@GetMapping("edit/{id}")
	public String showUpdateForm(@PathVariable("id") long id, Model model) {
		Instrument instrument = instrumentService.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid instrument Id:" + id));
		model.addAttribute("instrument", instrument);
		return "instruments/update-instrument";
	}
	
	@PostMapping("update/{id}")
	public String updateInstrument(@PathVariable("id") long id, @Valid Instrument instrument, BindingResult result, Model model) {
		try {
			if (result.hasErrors()) {
				instrument.setId(id);
				return "instruments/update-instrument";
			}		
		    Optional<Instrument> existingInstrument = instrumentService.findById(instrument.getId());
			if (existingInstrument.isPresent()){
				if (existingInstrument.get().getId() != instrument.getId()) {
		        	throw new EntityExistsException("The id " + instrument.getId() +" already exists in DB.");
		    	}
				instrument.setId(existingInstrument.get().getId());
				instrumentService.save(instrument);
				model.addAttribute("instruments", instrumentService.findAll());
				return "instruments/instrument-list";	
			} else {
				throw new EntityNotFoundException("The instrument with Id " + instrument.getId() +" doesn´t exist in DB.");
			}
		} catch (Exception e) {
			LOGGER.error("Error Message: " + e.getMessage());
			throw e;
		}
	}
	
	@DeleteMapping("{id}")
	public String deleteInstrument(@PathVariable("id") long id, Model model) {
		Instrument instrument = instrumentService.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid instrument Id:" + id));
		instrumentService.delete(instrument);
		model.addAttribute("instruments", instrumentService.findAll());
		return "instruments/instrument-list";
	}
	
	@GetMapping("bring-instruments/{id}")
	public String bringInstruments(@PathVariable("id") long id, Model model) {
		LOGGER.info("retrieving all instreument ...");
		Optional<Student> studentOpt = studentService.findByUsername(getPrincipal());
    	if (studentOpt.isPresent()) {
			long studentId = studentOpt.get().getId();
			model.addAttribute("studentId", studentId);
    	}
    	List<Instrument> myInstruments = getInstruments(id);
		model.addAttribute("myInstruments", myInstruments);
		return "instruments/my-instruments";
	}
	
	private List<Instrument> getInstruments(Long id) {
		if (!instrumentService.isAlreadyExistingInstrument_id(id)) {
			throw new EntityNotFoundException("The instrument whit id " + id + " doesn´t exist in DB.");
		}
		Student student = studentService.findById(id).get();
		return student.getInstrumets();
	}
}