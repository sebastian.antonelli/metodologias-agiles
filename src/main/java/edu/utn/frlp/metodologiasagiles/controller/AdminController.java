package edu.utn.frlp.metodologiasagiles.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.utn.frlp.metodologiasagiles.dto.InscriptionDTO;
import edu.utn.frlp.metodologiasagiles.dto.RentalDTO;
import edu.utn.frlp.metodologiasagiles.entity.Instrument;
import edu.utn.frlp.metodologiasagiles.entity.Student;
import edu.utn.frlp.metodologiasagiles.entity.Subject;
import edu.utn.frlp.metodologiasagiles.service.InstituteService;
import edu.utn.frlp.metodologiasagiles.service.InstrumentService;
import edu.utn.frlp.metodologiasagiles.service.StudentService;
import edu.utn.frlp.metodologiasagiles.service.SubjectService;

@Controller
@RequestMapping("/admin/")
public class AdminController {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(AdminController.class);

    @Autowired
	InstrumentService instrumentService;
    
    @Autowired
	SubjectService subjectService;
    
    @Autowired
	InstituteService instituteService;
    
    @Autowired
 	StudentService studentService;

	@GetMapping("inscriptions")
	public String getAllInscriptions(Model model) {
		LOGGER.info("retrieving all inscriptions...");
		List<InscriptionDTO> inscriptions = getInscriptions();
		model.addAttribute("inscriptions", inscriptions);
		return "admin/inscription-list";
	}

	private List<InscriptionDTO> getInscriptions() {
		List<InscriptionDTO> inscriptions = new ArrayList<InscriptionDTO>();
		Iterable<Student> students = studentService.findAll();
		
		for(Student student : students) {
			if (student.getSubjects() != null && student.getSubjects().size() > 0) {
				for (Subject subject : student.getSubjects()) {
					InscriptionDTO inscriptionDTO = new InscriptionDTO();
					inscriptionDTO.setStudentName(student.getFirstName() + " " + student.getLastName());
					inscriptionDTO.setUsername(student.getUser().getUsername());
					inscriptionDTO.setEmail(student.getUser().getEmail());
					inscriptionDTO.setSubjectName(subject.getName());
					inscriptionDTO.setSubjectDays(subject.getDays().toString());
					inscriptions.add(inscriptionDTO);
				}
			}
		}
		return inscriptions;
	}
	
	@GetMapping("rentals")
	public String getAllRentals(Model model) {
		LOGGER.info("retrieving all rentals...");
		List<RentalDTO> rentals = getRentals();
		model.addAttribute("rentals", rentals);
		return "admin/rental-list";
	}

	private List<RentalDTO> getRentals() {
		List<RentalDTO> rentals = new ArrayList<RentalDTO>();
		Iterable<Student> students = studentService.findAll();
		
		for(Student student : students) {
			if (student.getInstrumets() != null && student.getInstrumets().size() > 0) {
				for (Instrument instrument : student.getInstrumets()) {
					RentalDTO rentalDTO = new RentalDTO();
					rentalDTO.setStudentName(student.getFirstName() + " " + student.getLastName());
					rentalDTO.setUsername(student.getUser().getUsername());
					rentalDTO.setEmail(student.getUser().getEmail());
					rentalDTO.setInstrumentName(instrument.getName());
					rentalDTO.setInstrumentCost(instrument.getRentalRate());
					rentals.add(rentalDTO);
				}
			}
		}
		return rentals;
	}
	
	@GetMapping("metrics")
	public String getMetrics(Model model) {
		LOGGER.info("retrieving metrics page...");
		return "admin/metrics";
	}
	
	@GetMapping("main-values")
	public @ResponseBody Map<String,String> returnMainValues(Model model) {
		try {
			LOGGER.info("showing metrics...");
			Map<String,String> metrics = new HashMap<String,String>();
			Integer totalInscriptions = getInscriptions().size();
			Integer totalRentals = getRentals().size();
			List<Subject> subjects = (List<Subject>) subjectService.findAll();
			List<Student> students = (List<Student>) studentService.findAll();
			List<Instrument> instruments = (List<Instrument>) instrumentService.findAll();
			Float totalRents = instituteService.findById(1L).get().getTotalRentals();
			metrics.put("totalInscriptions", totalInscriptions.toString());
			metrics.put("totalRentals", totalRentals.toString());
			metrics.put("subjectsCount", Integer.toString(subjects.size()));
			metrics.put("studentsCount", Integer.toString(students.size()));
			metrics.put("instrumentsCount", Integer.toString(instruments.size()));
			metrics.put("totalRents", totalRents.toString());
	        return metrics;
		} catch (Exception e) {
			LOGGER.error("Error Message: " + e.getMessage());
			throw e;
		}
	}

	@GetMapping("students-per-subject")
	public @ResponseBody Map<String,Integer> studentsPerSubject(Model model) {
		try {
			LOGGER.info("showing students per subject...");
			List<Subject> subjects = (List<Subject>) subjectService.findAll();
	    	Map<String, Integer> studentsPerSubject = new HashMap<String, Integer>();
	    	for (Subject subject : subjects) {
	    		studentsPerSubject.put(subject.getName(), subject.getStudents().size());
	    	}
	        return studentsPerSubject;
		} catch (Exception e) {
			LOGGER.error("Error Message: " + e.getMessage());
			throw e;
		}
	}
	
	@GetMapping("rents-per-instrument")
	public @ResponseBody Map<String,Long> rentsPerInstrument(Model model) {
		try {
			LOGGER.info("showing rents per instrument...");
			List<Instrument> instruments = (List<Instrument>) instrumentService.findAll();
	    	Map<String, Long> rentsPerInstruments = new HashMap<String, Long>();
	    	for (Instrument instrument : instruments) {
	    		rentsPerInstruments.put(instrument.getName(), instrument.getRentalCount());
	    	}
	        return rentsPerInstruments;
		} catch (Exception e) {
			LOGGER.error("Error Message: " + e.getMessage());
			throw e;
		}
	}
	
	@GetMapping("spent-money-per-student")
	public @ResponseBody Map<String,Float> spentMoneyPerStudents(Model model) {
		try {
			LOGGER.info("showing spent money per students...");
			List<Student> students = (List<Student>) studentService.findAll();
	    	Map<String, Float> spentMoneyPerStudent = new HashMap<String, Float>();
	    	for (Student student : students) {
	    		spentMoneyPerStudent.put(student.getFirstName() + " " + student.getLastName() , student.getSpentMoney());
	    	}
	        return spentMoneyPerStudent;
		} catch (Exception e) {
			LOGGER.error("Error Message: " + e.getMessage());
			throw e;
		}
	}

}