package edu.utn.frlp.metodologiasagiles.controller;


import static edu.utn.frlp.metodologiasagiles.security.UserPrincipal.getPrincipal;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.utn.frlp.metodologiasagiles.entity.Student;
import edu.utn.frlp.metodologiasagiles.entity.Subject;
import edu.utn.frlp.metodologiasagiles.service.StudentService;
import edu.utn.frlp.metodologiasagiles.service.SubjectService;

@Controller
@RequestMapping("/subjects/")
public class SubjectController {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(SubjectController.class);

    @Autowired
	SubjectService subjectService;
    
    @Autowired
 	StudentService studentService;
	
	@GetMapping("add")
	public String showSignUpForm(Subject Subject) {
		return "subjects/add-subject";
	}

	
	@GetMapping("list")
	public String getAllSubjects(Model model) {
		Optional<Student> studentOpt = studentService.findByUsername(getPrincipal());
		List<Long> subjectsIds = new ArrayList<Long>();
		if (studentOpt.isPresent()) {
			long studentId = studentOpt.get().getId();
			model.addAttribute("studentId", studentId);
			for (Subject subject : studentOpt.get().getSubjects()) {
				Long subjectId= subject.getId();
				subjectsIds.add(subjectId);
			}
		} else {
			model.addAttribute("studentId", 0);
		}
		model.addAttribute("studentSubjectIds", subjectsIds);
		model.addAttribute("subjects", subjectService.findAll());
		return "subjects/subject-list";
	}

	
	@PostMapping("add")
	public String addSubject(@Valid Subject subject, BindingResult result, Model model) {
		try {
			if (result.hasErrors()) {
				return "subjects/add-subject";
			}

	    	if (subjectService.isAlreadyExistingSubject(subject.getName() )) {
	       	throw new EntityExistsException("The subject with name " + subject.getName() +  
					        			" already exists in DB.");
	    	}
	    	subjectService.save(subject);
			return "redirect:list";
		} catch (Exception e) {
			LOGGER.error("Error Message: " + e.getMessage());
			throw e;
		}
	}

	
	@GetMapping("edit/{id}")
	public String showUpdateForm(@PathVariable("id") long id, Model model) {
		Subject subject = subjectService.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid subject Id:" + id));
		model.addAttribute("subject", subject);
		return "subjects/update-subject";
	}

	
	@PostMapping("update/{id}")
	public String updateSubject(@PathVariable("id") long id, @Valid Subject subject, BindingResult result, Model model) {
		try {
			if (result.hasErrors()) {
				subject.setId(id);
				return "subjects/update-subject";
			}

			subjectService.save(subject);
			model.addAttribute("subjects", subjectService.findAll());
			return "subjects/subject-list";
		} catch (Exception e) {
			LOGGER.error("Error Message: " + e.getMessage());
			throw e;
		}
	}

	@Transactional
	@DeleteMapping("{id}")
	public String deleteMateria(@PathVariable("id") long id, Model model) {
		Subject subject = subjectService.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid subject Id:" + id));
		subject.getStudents().forEach(st -> st.getSubjects().remove(subject));
		studentService.saveAll(subject.getStudents());
		subjectService.delete(subject);

		model.addAttribute("subjects", subjectService.findAll());
		return "subjects/subject-list";
	}
	
	@GetMapping("inscription/{studentId}/{subjectId}")
	public String subjectInscription(@PathVariable("studentId") long studentId, @PathVariable("subjectId") long subjectId, Model model) {
		try {
			
	    	if (!subjectService.isAlreadyExistingSubject_Id(subjectId)) {
	        	throw new EntityNotFoundException("The subject with id " + subjectId + " doesn´t exist in DB.");
	    	}
	    	
	    	if (!studentService.isAlreadyExistingStudent(studentId)) {
	        	throw new EntityNotFoundException("The student with id " + studentId + " doesn´t exist in DB.");
	    	}
	    	
	    	subjectService.subjectInscription(studentId, subjectId);
	    	model.addAttribute("success", "You are enrolled in this subject.");
	        model.addAttribute("url", "subjects/list");
	        return "success";
		} 
		catch (Exception e) {
			LOGGER.error("Error Message: " + e.getMessage());
			throw e;
		}
	}
	
	@GetMapping("unsubscribe/{studentId}/{subjectId}")
	public String unsubscribeSubject(@PathVariable("studentId") long studentId, @PathVariable("subjectId") long subjectId, Model model) {
		try {
			
			if (!subjectService.isAlreadyExistingSubject_Id(subjectId)) {
				throw new EntityNotFoundException("The subject with id " + subjectId + " doesn´t exist in DB.");
			}
			
			if (!studentService.isAlreadyExistingStudent(studentId)) {
				throw new EntityNotFoundException("The student whit id " + studentId + " doesn´t exist in DB.");
			}
			
			subjectService.unsubscribeSubject(studentId, subjectId);
			model.addAttribute("success", "You unsubscribed correctly");
			model.addAttribute("url", "subjects/list");
			return "success";
		} catch (Exception e) {
			LOGGER.error("Error Message: " + e.getMessage());
			throw e;
		}
	}
	
	@GetMapping("bring-subjects/{id}")
	public String bringSubjects(@PathVariable("id") long id, Model model) {
		LOGGER.info("retrieving all subjects ...");
		Optional<Student> studentOpt = studentService.findByUsername(getPrincipal());
    	if (studentOpt.isPresent()) {
			long studentId = studentOpt.get().getId();
			model.addAttribute("studentId", studentId);
    	}
		List<Subject> mySubjects = getSubjects(id);
		model.addAttribute("mySubjects", mySubjects);
		return "subjects/my-subjects";
	}
	
	private List<Subject> getSubjects(Long id) {
		if (!studentService.isAlreadyExistingStudent(id)) {
			throw new EntityNotFoundException("The student whit id " + id + " doesn´t exist in DB.");
		}
		Student student = studentService.findById(id).get();
		return student.getSubjects().stream().collect(Collectors.toList());
	}
}

