package edu.utn.frlp.metodologiasagiles.controller;

import static edu.utn.frlp.metodologiasagiles.security.UserPrincipal.getPrincipal;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import edu.utn.frlp.metodologiasagiles.entity.Student;
import edu.utn.frlp.metodologiasagiles.service.StudentService;
import edu.utn.frlp.metodologiasagiles.service.UserService;

@Controller
public class UserController {

    @Autowired
    AuthenticationTrustResolver authenticationTrustResolver;

    @Autowired
    UserService userService;
    
    @Autowired
    PasswordEncoder passwordEncoder;
    
    @Autowired
    StudentService studentService;

 
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    /**
     * Handles the home page redirection
     */
    @GetMapping({"/","/index"})
    public String welcomePage(Model model) {
        if (!isCurrentAuthenticationAnonymous()) {
        	Optional<Student> studentOpt = studentService.findByUsername(getPrincipal());
        	if (studentOpt.isPresent()) {
    			long studentId = studentOpt.get().getId();
    			model.addAttribute("studentId", studentId);
        	}
            return "index";
        } else {
            return "login";
        }
    }

    /**
     * Handles the login
     */
    @GetMapping(value = "/login")
    public String loginPage() {
//    	System.out.println(passwordEncoder.encode("password"));
    	LOGGER.info("Welcome!");
        return "login";
    }

    /**
     * Handles the logout
     */
    @GetMapping(value = "/logout")
    public String logoutPage(Model model) {
        model.addAttribute("title", "Logout");
        return "login";
    }

    /**
     * Handles the access denied page redirection
     */
    @GetMapping(value = "/access-denied")
    public String accessDeniedPage(Model model) {
        model.addAttribute("accessDenied", "The user is not allowed to access the contents of this page.");
        return "accessDenied";
    }

    /**
     * Returns true if user is already authenticated, else false
     * @return boolean
     */
    private boolean isCurrentAuthenticationAnonymous() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authenticationTrustResolver.isAnonymous(authentication);
    }
}
