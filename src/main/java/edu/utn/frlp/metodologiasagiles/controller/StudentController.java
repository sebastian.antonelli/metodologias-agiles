package edu.utn.frlp.metodologiasagiles.controller;

import java.util.Optional;

import javax.persistence.EntityExistsException;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.utn.frlp.metodologiasagiles.entity.Role;
import edu.utn.frlp.metodologiasagiles.entity.Student;
import edu.utn.frlp.metodologiasagiles.entity.User;
import edu.utn.frlp.metodologiasagiles.service.InstrumentService;
import edu.utn.frlp.metodologiasagiles.service.StudentService;
import edu.utn.frlp.metodologiasagiles.service.UserService;

@Controller
@RequestMapping("/students/")
public class StudentController {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(StudentController.class);

    @Autowired
	StudentService studentService;
    
    @Autowired
	InstrumentService instrumentService;
	
	@Autowired
	UserService userService;
	
	@Autowired
    PasswordEncoder passwordEncoder;


	@GetMapping("signup")
	public String showSignUpForm(Student student) {
		return "students/add-student";
	}

	
	@GetMapping("list")
	public String showUpdateForm(Model model) {
		model.addAttribute("students", studentService.findAll());
		return "students/student-list";
	}

	
	@PostMapping("add")
	public String addStudent(@Valid Student student, BindingResult result, Model model) {
		try {
			if (result.hasErrors()) {
				return "students/add-student";
			}

	    	if (userService.isAlreadyExistingUser(student.getUser().getUsername(), student.getUser().getEmail())) {
	        	throw new EntityExistsException("The user with username " + student.getUser().getUsername() + 
					        			" and email " + student.getUser().getEmail() + 
					        			" already exists in DB.");
	    	}
	    	student.getUser().setPassword(passwordEncoder.encode("password"));
	    	student.getUser().setRole(Role.STUDENT.toString());
	    	student.setSpentMoney((float) 0);
	    	studentService.save(student);
			return "redirect:list";
		} catch (Exception e) {
			LOGGER.error("Error Message: " + e.getMessage());
			throw e;
		}
	}

	
	@GetMapping("edit/{id}")
	public String showUpdateForm(@PathVariable("id") long id, Model model) {
		Student student = studentService.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + id));
		model.addAttribute("student", student);
		return "students/update-student";
	}

	
	@PostMapping("update/{id}")
	public String updateStudent(@PathVariable("id") long id, @Valid Student student, BindingResult result, Model model) {
		try {
			if (result.hasErrors()) {
				student.setId(id);
				return "students/update-student";
			}
			User existingUser = userService.findByUsername(student.getUser().getUsername());
			Optional<User> userOpt = userService.findByEmail(student.getUser().getEmail());
			
			if (userOpt.isPresent() && !userOpt.get().getUsername().equals(student.getUser().getUsername())) {
	        	throw new EntityExistsException("The email " + student.getUser().getEmail() + 
					        					" already exists in DB.");
	    	}
			student.getUser().setPassword(existingUser.getPassword());

			studentService.save(student);
			model.addAttribute("students", studentService.findAll());
			return "students/student-list";
		} catch (Exception e) {
			LOGGER.error("Error Message: " + e.getMessage());
			throw e;
		}
	}

	@DeleteMapping("{id}")
	public String deleteStudent(@PathVariable("id") long id, Model model) {
		Student student = studentService.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + id));
		student.getInstrumets().forEach(i -> {
			i.setStudent(null);
			i.setState(true);
		});
		instrumentService.saveAll(student.getInstrumets());
		studentService.delete(student);
		model.addAttribute("students", studentService.findAll());
		return "students/student-list";
	}
}
