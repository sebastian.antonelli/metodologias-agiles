package edu.utn.frlp.metodologiasagiles.controller;

import static edu.utn.frlp.metodologiasagiles.security.UserPrincipal.getPrincipal;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.utn.frlp.metodologiasagiles.entity.Student;
import edu.utn.frlp.metodologiasagiles.service.StudentService;


@Controller
@RequestMapping("/commons/")
public class CommonsController {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(CommonsController.class);
    
    @Autowired
 	StudentService studentService;

	@GetMapping("contactus")
	public String getContactUsPage(Model model) {
		LOGGER.info("show the contact us page");
		Optional<Student> studentOpt = studentService.findByUsername(getPrincipal());
    	if (studentOpt.isPresent()) {
			long studentId = studentOpt.get().getId();
			model.addAttribute("studentId", studentId);
    	}
		return "contact-us";
	}
	
	@GetMapping("help")
	public String getHelpPage(Model model) {
		LOGGER.info("show the help page");
		
		Optional<Student> studentOpt = studentService.findByUsername(getPrincipal());
    	if (studentOpt.isPresent()) {
			long studentId = studentOpt.get().getId();
			model.addAttribute("studentId", studentId);
    	}
		return "help";
	}
}

	

