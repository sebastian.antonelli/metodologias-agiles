package edu.utn.frlp.metodologiasagiles.exception;

import javax.persistence.EntityExistsException;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
class GlobalExceptionHandler {

	@ExceptionHandler({Exception.class,
			UsernameNotFoundException.class,
			EntityExistsException.class})
	public ModelAndView defaultErrorHandler(Exception e) {
		ModelAndView model = new ModelAndView();
		model.addObject("error", e.getClass().getName());
		model.addObject("message", e.getMessage());
		model.setViewName("error");
		return model;
	}
}