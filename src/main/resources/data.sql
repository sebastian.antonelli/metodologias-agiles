INSERT INTO user (id, username, password, email, role) VALUES
  (1, 'ada.lovelace', '$2a$10$Klb8f8g17f/HN/oDQy5FL.EFd.YXdHxeK2mnj1cRVdweLBoUozj.e', 'ada.lovelace@gmail.com', 'STUDENT'),
  (2, 'bill.gates', '$2a$10$Klb8f8g17f/HN/oDQy5FL.EFd.YXdHxeK2mnj1cRVdweLBoUozj.e', 'bill.gates@gmail.com', 'STUDENT'),
  (3, 'emilio.watemberg', '$2a$10$Klb8f8g17f/HN/oDQy5FL.EFd.YXdHxeK2mnj1cRVdweLBoUozj.e', 'emilio.watemberg@gmail.com', 'TEACHER'),
  (4, 'admin', '$2a$10$Klb8f8g17f/HN/oDQy5FL.EFd.YXdHxeK2mnj1cRVdweLBoUozj.e', 'admin@admin.com', 'ADMINISTRATOR'),
  (5, 'sebastian.antonelli', '$2a$10$Klb8f8g17f/HN/oDQy5FL.EFd.YXdHxeK2mnj1cRVdweLBoUozj.e', 'sebastian.antonelli@gmail.com', 'STUDENT'),
  (6, 'elias.petriella', '$2a$10$Klb8f8g17f/HN/oDQy5FL.EFd.YXdHxeK2mnj1cRVdweLBoUozj.e', 'elias.petriella@gmail.com', 'STUDENT'),
  (7, 'abigail.gonzalez', '$2a$10$Klb8f8g17f/HN/oDQy5FL.EFd.YXdHxeK2mnj1cRVdweLBoUozj.e', 'abigail.gonzalez@gmail.com', 'STUDENT'),
  (8, 'enzo.avila', '$2a$10$Klb8f8g17f/HN/oDQy5FL.EFd.YXdHxeK2mnj1cRVdweLBoUozj.e', 'enzo.avila@gmail.com', 'STUDENT'),
  (9, 'camila.domian', '$2a$10$Klb8f8g17f/HN/oDQy5FL.EFd.YXdHxeK2mnj1cRVdweLBoUozj.e', 'camila.domian@gmail.com', 'STUDENT'),
  (10, 'ruben.denis', '$2a$10$Klb8f8g17f/HN/oDQy5FL.EFd.YXdHxeK2mnj1cRVdweLBoUozj.e', 'ruben.denis@gmail.com', 'STUDENT');


INSERT INTO student (id, first_name, last_name, dni, code, phone_no, spent_money, user_id) VALUES
  (1, 'Ada', 'Lovelace', 54789612, 145478, 232234152, 1950.0, 1),
  (2, 'Bill', 'Gates', 547896214, 125498, 351125154, 1760.0, 2),
  (3, 'Sebastian', 'Antonelli', 45112412, 123343, 231251244, 2650.0, 5),
  (4, 'Elias', 'Petriella', 45112477, 155343, 231251222, 1900.0, 6),
  (5, 'Abigail', 'Gonzalez', 45113331, 1232343, 231251333, 2700.0, 7),
  (6, 'Camila', 'Domian', 46532241, 123113, 231251444, 2350.0, 9),
  (7, 'Enzo', 'Avila', 45164541, 123552, 231251555, 2750.0, 8),
  (8, 'Ruben', 'Denis', 45342412, 123366, 231251666, 2100.0, 10);

  
INSERT INTO subject (id, name, start_time, duration) VALUES
  (1, 'Curso inicial de Guitarra', 14.30 , 2.30),
  (2, 'Curso de Percusion', 17, 2.30),
  (3, 'Taller de Coro', 10.30, 2),
  (4, 'Curso de Bateria', 9.30, 2),
  (5, 'Curso de Bajo', 12.30, 2),
  (6, 'Curso avanzado de Guitarra', 14.30, 2);
  
INSERT INTO instrument (id, name, rental_rate, state, rental_count, student_id) VALUES
  (1, 'Guitarra Acustica Fender', 450, true, 4, null),
  (2, 'Guitarra Electrica Taylor', 550, true, 4, null),
  (3, 'Saxofon Parquer', 750, false, 3, 2),
  (4, 'Bongo Stagg', 280, true, 9, null),
  (5, 'Bajo Electrico Leonard', 430, false, 5, 5),
  (6, 'Bateria Octapad Yamaha', 800, true, 5, null),
  (7, 'Ukulele Electroacustico Ibanez', 390, true, 6, null),
--  (8, 'Bateria Mapex', 1190, true, 3, null),
--  (9, 'Flauta traversa Parquer', 625, true, 4, null),
--  (10, 'Conga Parquer Wonder', 300, true, 5, null),
  (11, 'Bajo Electrico Parquer', 500, true, 2, null);

INSERT INTO students_subjects (student_id, subject_id)  VALUES
  (2, 1),
  (1, 1),
  (1, 2);
  
INSERT INTO institute (id, name, total_rentals) VALUES
  (1, 'Music All In', 18260.00);
  
 INSERT INTO subject_days (subject_id, days)  VALUES
  (2, 'MARTES'),
  (2, 'JUEVES'),
  (3, 'LUNES'),
  (1, 'VIERNES'),
  (3, 'LUNES'),
  (4, 'MIERCOLES'),
  (4, 'JUEVES'),
  (5, 'VIERNES'),
  (6, 'MARTES'),
  (6, 'MIERCOLES');