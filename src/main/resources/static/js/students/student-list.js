$(document).ready(function() {

    $('#studentsTable').DataTable({
		"scrollXInner": true,
        "scrollYInner": "100%",
        columnDefs: [ { targets: [ 6 ], 'bSortable': false } ],
        dom:'<"row"<"left aligned col-sm-6"f><"right aligned text-right col-sm-6"l>>' +
			"<'row'<'col-sm-12'tr>>" +
			"<'row'<'left aligned col-sm-6'i><'left aligned col-sm-6'p>>"
	});

});
