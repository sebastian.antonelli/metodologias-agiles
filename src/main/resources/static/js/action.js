$(document).ready( function () {

    $('.goto').click(function (e) {
        $.blockUI({
            message: 'Please wait...',
            fadeIn: 0,
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: 0.5,
                color: '#fff'
            }
        });
    });

});