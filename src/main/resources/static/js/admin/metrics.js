$(document).ready(function() {	
	
	function shuffle(a) {
	  var j, x, i;
	  for (i = a.length - 1; i > 0; i--) {
	      j = Math.floor(Math.random() * (i + 1));
	      x = a[i];
	      a[i] = a[j];
	      a[j] = x;
	  }
	  return a;
	}
	
	const colours = ['#FFFF00', '#FFFFCC', 
					'#CCFF00', '#CCFF99', 
					'#66CC00', '#009933',
					'#CC9900','#663300', 
					'#FF9900', '#FFCC99', 
					'#FF0000', '#FF6666', 
					'#FF00FF', '#FF99FF', 
					'#9900FF', '#9999FF',
					'#0000FF', '#00CCFF', 
					'#00FFFF', '#003366'];
					
	var colorsGraphOne = colours.slice();
	colorsGraphOne = shuffle(colorsGraphOne);
	
	var colorsGraphTwo = colours.slice();
	colorsGraphTwo = shuffle(colorsGraphTwo);
	
	var colorsGraphThree = colours.slice();
	colorsGraphThree = shuffle(colorsGraphThree);	
					
	function executeMethods() {
		getMainValues();
		getStudentsPerSubject();
		getRentsPerInstrument();
		getSpentMoneyPerStudent();
	}
	
	executeMethods();
	
	setInterval(function() {
        executeMethods();
		
    }, 15000);

	

// Se obtienen todos los valores principales
	function getMainValues() {
		$.ajax({
			type: "GET",
			url: "main-values",
			datatype: "json",
			success: function (data) {
				var studentsCount = document.getElementById('studentsCount');
				var subjectsCount = document.getElementById('subjectsCount');
				var instrumentsCount = document.getElementById('instrumentsCount');
				var totalInscriptions = document.getElementById('totalInscriptions');
				var totalRentals = document.getElementById('totalRentals');
				var totalRents = document.getElementById('totalRents');
				studentsCount.innerHTML = data.studentsCount;
				subjectsCount.innerHTML = data.subjectsCount;
				instrumentsCount.innerHTML = data.instrumentsCount;
				totalInscriptions.innerHTML = data.totalInscriptions;
				totalRentals.innerHTML = data.totalRentals;
				totalRents.innerHTML = '$ ' + data.totalRents;
			},
			error: function (data){
				alert('Hubo un error al recuperar los valores principales.');
			}
		})
	}		
	
// Se obtienen todos los students por subject
	function getStudentsPerSubject() {
		$.ajax({
			type: "GET",
			url: "students-per-subject",
			datatype: "json",
			success: function (data) {
				createStudentsPerSubject(data);
			},
			error: function (data){
				alert('Hubo un error al recuperar la cantidad de students por subject en el gr\u00E1fico.');
			}
		})
	}
	
	// Se obtienen todos los alquileres por instrument
	function getRentsPerInstrument() {
		$.ajax({
			type: "GET",
			url: "rents-per-instrument",
			datatype: "json",
			success: function (data) {
				createRentsPerInstruments(data);
			},
			error: function (data){
				alert('Hubo un error al recuperar la cantidad de alquileres por instrumento en el gr\u00E1fico.');
			}
		})
	}
	
	// Se obtienen todos los montos de alquileres gastados por student
	function getSpentMoneyPerStudent() {
		$.ajax({
			type: "GET",
			url: "spent-money-per-student",
			datatype: "json",
			success: function (data) {
				createSpentMoneyPerStudent(data);
			},
			error: function (data){
				alert('Hubo un error al recuperar los montos de alquileres por estudiante en el gr\u00E1fico.');
			}
		})
	}
	
	function createStudentsPerSubject(mapStudentsPerSubject){
		var labels = [];
		var values = [];
		var colors = [];	
		var colorsCopy = colorsGraphOne.slice();	
		for (const [key,value] of Object.entries(mapStudentsPerSubject)){
			labels.push(key);
			values.push(value);
			
      		const el = colorsCopy[0];
			colorsCopy.shift();

			colors.push(el);
		}
		var studentsPerSubjectData = {
			type : "pie",
			data : {
				datasets : [{
					data : values,
					backgroundColor : colors
				}],
				labels : labels
			}
		}
		var studentsPerSubjectCanvas = document.getElementById('studentsPerSubject').getContext('2d');
		window.pie = new Chart(studentsPerSubjectCanvas,studentsPerSubjectData);
		window.pie.update();
	}
	
	
	function createRentsPerInstruments(mapRentsPerInstruments){
		var labels = [];
		var values = [];
		var colors = [];
		var colorsCopy = colorsGraphTwo.slice();	
		for (const [key,value] of Object.entries(mapRentsPerInstruments)){
			labels.push(key);
			values.push(value);
						
      		const el = colorsCopy[0];
			colorsCopy.shift();
			colors.push(el);
		}
		var rentsPerInstrumentData = {
			type : "pie",
			data : {
				datasets : [{
					data : values,
					backgroundColor : colors
				}],
				labels : labels
			}
		}
		var rentsPerInstrumentCanvas = document.getElementById('rentsPerInstruments').getContext('2d');
		window.pie2 = new Chart(rentsPerInstrumentCanvas,rentsPerInstrumentData);
		window.pie2.update();
	}
	
	
	function createSpentMoneyPerStudent(mapSpentMoneyPerStudent){
		var labels = [];
		var values = [];
		var colors = [];
		var colorsCopy = colorsGraphThree.slice();	
		for (const [key,value] of Object.entries(mapSpentMoneyPerStudent)){
			labels.push(key);
			values.push(value);
						
      		const el = colorsCopy[0];
			colorsCopy.shift();
			colors.push(el);
		}
		var spentMoneyPerStudentData = {
			type : "horizontalBar",
			data : {
				datasets : [{
					data : values,
					backgroundColor : colors
				}],
				labels : labels
			},
			options: {
			    legend: {
			        display: false
			    },
				scales: {
			        xAxes: [{
			            ticks: {
			                beginAtZero: true
			            }
			        }],
					yAxes:[{
						    barPercentage: 0.5,
							categoryPercetange: 0.5,
							maxBarThickness: 40
					}]
			    }
			}
		}
		var spentMoneyPerStudentCanvas = document.getElementById('spentMoneyPerStudent').getContext('2d');
		spentMoneyPerStudentCanvas.height = 250;
		window.bar = new Chart(spentMoneyPerStudentCanvas,spentMoneyPerStudentData);
		window.bar.update();
	}
});
