$(document).ready(function() {

    $('#sidebar-toggle-button-full').on('click', function () {
        var element = document.getElementById("sidebar");
        if ($(element).hasClass("sidebar-lg-show"))
            element.classList.remove("sidebar-lg-show");
        else {
            element.classList.add("sidebar-lg-show");
        }
    });

    $('#sidebar-toggle-button-min').on('click', function () {
        var element = document.getElementById("sidebar");
        if ($(element).hasClass("sidebar-show"))
            element.classList.remove("sidebar-show");
        else {
            element.classList.add("sidebar-show");
        }
    });

    $('#sidebar-toggle-button-minimizer').on('click', function () {
        var element = document.getElementById("sidebar");
        var elementSidebarNav = document.getElementById("sidebar-nav");
        if ($(element).hasClass("brand-minimized") && $(element).hasClass("sidebar-minimized")) {
            elementSidebarNav.classList.add(("ps"));
            elementSidebarNav.classList.add(("ps--active-y"));
            element.classList.remove("brand-minimized");
            element.classList.remove("sidebar-minimized");
        } else {
            elementSidebarNav.classList.remove(("ps"));
            elementSidebarNav.classList.remove(("ps--active-y"));
            element.classList.add("brand-minimized");
            element.classList.add("sidebar-minimized");
        }
    });

    $('#sidebar-dropdown-students').on('click', function () {
        var element = document.getElementById("sidebar-dropdown-students");
        if ($(element).hasClass("open")) {
            element.classList.remove("open");
        } else {
            element.classList.add("open");
        }
    });
	 $('#sidebar-dropdown-instruments').on('click', function () {
        var element = document.getElementById("sidebar-dropdown-instruments");
        if ($(element).hasClass("open")) {
            element.classList.remove("open");
        } else {
            element.classList.add("open");
        }
    });

    $('#sidebar-dropdown-subjects').on('click', function () {
        var element = document.getElementById("sidebar-dropdown-subjects");
        if ($(element).hasClass("open")) {
            element.classList.remove("open");
        } else {
            element.classList.add("open");
        }
    });

/**    $('#sidebar-dropdown-settings').on('click', function () {
        var element = document.getElementById("sidebar-dropdown-settings");
        if ($(element).hasClass("open")) {
            element.classList.remove("open");
        } else {
            element.classList.add("open");
        }
    });
    */

});
