# Metodologías Ágiles - TP

**Tools and Technologies Used**

Spring Boot - 2.1.4 RELEASE

Spring Framework - 5.1.6 RELEASE

Spring Data JPA - 2.16 RELEASE

Hibernate - 5.3.9.Final

Thymeleaf - 3.0.11 RELEASE

Maven - 3.2+

IDE - Eclipse or Spring Tool Suite (STS)  / VS Code

H2 Database - 1.4.99

